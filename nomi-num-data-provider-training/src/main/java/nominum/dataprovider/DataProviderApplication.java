package nominum.dataprovider;

import nominum.dataprovider.rest.data.Course;
import nominum.dataprovider.rest.data.User;
import nominum.dataprovider.repository.CoursesRepository;
import nominum.dataprovider.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("nominum.dataprovider")
@EntityScan(basePackages={"nominum.dataprovider.rest.data"})
@EnableJpaRepositories(basePackages={"nominum.dataprovider.repository"})
public class DataProviderApplication implements CommandLineRunner {


  private UserRepository userRepository;
  private CoursesRepository coursesRepository;

  @Autowired
  public DataProviderApplication(UserRepository userRepository,
      CoursesRepository coursesRepository) {
    this.userRepository = userRepository;
    this.coursesRepository = coursesRepository;
  }

  public static void main(String[] args) {
		SpringApplication.run(DataProviderApplication.class, args);
	}

	public void run(String... args){

    User userHasAGrade = new User(1);
    User userHasFGrade = new User(1);
    userRepository.save(userHasAGrade);
    userRepository.save(userHasFGrade);

    Course courseOne =  new Course("Course1","A",userHasAGrade);
    Course courseTwo =  new Course("Course2","F",userHasFGrade);
    Course courseThree =  new Course("Course3","A",userHasAGrade);
    coursesRepository.save(courseOne);
    coursesRepository.save(courseTwo);
    coursesRepository.save(courseThree);
  }
}

