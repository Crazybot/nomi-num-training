package nominum.dataprovider.controller;

import nominum.dataprovider.rest.model.UserModel;
import nominum.dataprovider.services.DataProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@EnableAutoConfiguration
public class DataProviderController {

  @Autowired
  private DataProviderService dataProviderService;

  @GetMapping("/data/{userId}")
  public ResponseEntity getUser(@PathVariable Long userId) {
    UserModel userModel = dataProviderService.getUserModel(userId);
    if (userModel == null) {
      return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity(userModel, HttpStatus.OK);
  }
}
