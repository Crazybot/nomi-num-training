package nominum.dataprovider.repository;

import java.util.List;
import nominum.dataprovider.rest.data.Course;
import nominum.dataprovider.rest.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursesRepository extends JpaRepository<Course, Long>{
  Course findById(int courseId);

  List<Course> findCoursesByUser(User user);
}
