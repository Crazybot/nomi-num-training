package nominum.dataprovider.repository;

import nominum.dataprovider.rest.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

  User findById(long userId);
}
