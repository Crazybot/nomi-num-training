package nominum.dataprovider.rest.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Course {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String courseName;
  private String courseMark;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public Course() {
  }

  public Course(String courseName, String courseMark, User user) {
    this.courseName = courseName;
    this.courseMark = courseMark;
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseMark() {
    return courseMark;
  }

  public void setCourseMark(String courseMark) {
    this.courseMark = courseMark;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "Course{" +
        "id=" + id +
        ", courseName='" + courseName + '\'' +
        ", courseMark='" + courseMark + '\'' +
        ", user=" + user +
        '}';
  }
}
