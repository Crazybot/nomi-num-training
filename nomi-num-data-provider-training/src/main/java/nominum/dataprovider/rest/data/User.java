package nominum.dataprovider.rest.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User {

  public User() {
  }

  public User(int coursesCount) {
    this.coursesCount = coursesCount;
  }

  @Id
  @GeneratedValue
  private Long id;
  private int coursesCount;

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", coursesCount=" + coursesCount +
        '}';
  }

  public Long getId() {
    return id;
  }

  public int getCoursesCount() {
    return coursesCount;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setCoursesCount(int coursesCount) {
    this.coursesCount = coursesCount;
  }
}
