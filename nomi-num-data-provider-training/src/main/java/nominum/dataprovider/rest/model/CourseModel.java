package nominum.dataprovider.rest.model;

public class CourseModel {
    private String name;
    private String grade;


  public CourseModel() {
  }

  public CourseModel(String name, String grade) {
    this.name = name;
    this.grade = grade;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CourseModel that = (CourseModel) o;

    if (name != null ? !name.equals(that.name) : that.name != null) {
      return false;
    }
    return grade != null ? grade.equals(that.grade) : that.grade == null;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (grade != null ? grade.hashCode() : 0);
    return result;
  }
}
