package nominum.dataprovider.rest.model;

import java.util.List;

public class UserModel {
  private Long user;
  private int total;
  private List<CourseModel> courses;

  public UserModel() {
  }

  public UserModel(Long user, int total, List<CourseModel> courses) {
    this.user = user;
    this.total = total;
    this.courses = courses;
  }

  public Long getUser() {
    return user;
  }

  public void setUser(Long user) {
    this.user = user;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public List<CourseModel> getCourses() {
    return courses;
  }

  public void setCourses(List<CourseModel> courses) {
    this.courses = courses;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserModel userModel = (UserModel) o;

    if (total != userModel.total) {
      return false;
    }
    if (user != null ? !user.equals(userModel.user) : userModel.user != null) {
      return false;
    }
    return courses != null ? courses.equals(userModel.courses) : userModel.courses == null;
  }

  @Override
  public int hashCode() {
    int result = user != null ? user.hashCode() : 0;
    result = 31 * result + total;
    result = 31 * result + (courses != null ? courses.hashCode() : 0);
    return result;
  }
}
