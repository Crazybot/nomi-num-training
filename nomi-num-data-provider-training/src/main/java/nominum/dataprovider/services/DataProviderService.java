package nominum.dataprovider.services;

    import java.util.List;
    import nominum.dataprovider.rest.data.Course;
    import nominum.dataprovider.rest.data.User;
    import nominum.dataprovider.rest.model.UserModel;

public interface DataProviderService {

  User getUser(long userId);

  List<Course> getUserCourses(User user);

  UserModel getUserModel(Long userId);
}
