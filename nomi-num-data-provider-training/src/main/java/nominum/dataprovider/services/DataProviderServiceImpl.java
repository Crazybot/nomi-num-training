package nominum.dataprovider.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import nominum.dataprovider.rest.data.Course;
import nominum.dataprovider.rest.data.User;
import nominum.dataprovider.repository.CoursesRepository;
import nominum.dataprovider.repository.UserRepository;
import nominum.dataprovider.rest.model.CourseModel;
import nominum.dataprovider.rest.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataProviderServiceImpl implements DataProviderService {

  private UserRepository userRepository;
  private CoursesRepository coursesRepository;

  public DataProviderServiceImpl() {
  }

  @Autowired
  public DataProviderServiceImpl(UserRepository userRepository,
      CoursesRepository coursesRepository) {
    this.userRepository = userRepository;
    this.coursesRepository = coursesRepository;
  }

  public User getUser(long userId) {
    Optional<User> byId = Optional.ofNullable(userRepository.findById(userId));
    return byId.get();
  }

  public List<Course> getUserCourses(User user) {
    return coursesRepository.findCoursesByUser(user);
  }

  public UserModel getUserModel(Long userId) {
    User user = getUser(userId);
    List<Course> courses = getUserCourses(user);
    List<CourseModel> coursesModel = new ArrayList<>();
    courses.forEach(v -> {
      coursesModel.add(new CourseModel(v.getCourseName(), v.getCourseMark()));
    });
    return new UserModel(user.getId(), user.getCoursesCount(), coursesModel);
  }

}
