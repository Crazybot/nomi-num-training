package nominum.nominumdataprovidertraining;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import nominum.dataprovider.repository.CoursesRepository;
import nominum.dataprovider.repository.UserRepository;
import nominum.dataprovider.rest.data.Course;
import nominum.dataprovider.rest.data.User;
import nominum.dataprovider.rest.model.CourseModel;
import nominum.dataprovider.rest.model.UserModel;
import nominum.dataprovider.services.DataProviderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = DataProviderServiceImplTest.class)
public class DataProviderServiceImplTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private CoursesRepository coursesRepository;
  @Mock
  private User userFromDB;
  @Mock
  private List<Course> coursesFromDB;
  @InjectMocks
  private DataProviderServiceImpl dataProviderService;

  @Test
  public void getUser_ShouldReturnUser_WhileIdGiven() {
    //Given
    long userId = 1;
    when(userRepository.findById(userId)).thenReturn(userFromDB);
    //When
    dataProviderService.getUser(userId);
    //Then
    verify(userRepository, times(1)).findById(userId);
  }

  @Test
  public void getUserCourses_ShouldReturnListOfCourses_WhileUserGiven() {
    //Given
    when(coursesRepository.findCoursesByUser(userFromDB)).thenReturn(coursesFromDB);
    //When
    coursesRepository.findCoursesByUser(userFromDB);
    //Then
    verify(coursesRepository, times(1)).findCoursesByUser(userFromDB);
  }

  @Test
  public void getUserModel_ShouldReturnUserModel_GivenUserId() {
    //Given
    long userId = 1;
    User user = new User();
    user.setId(1L);
    user.setCoursesCount(1);
    Course course = new Course();
    course.setCourseName("Course A");
    course.setCourseMark("A");
    List<Course> courses = new ArrayList<>();
    List<CourseModel> coursesModel = new ArrayList<>();
    coursesModel.add(new CourseModel(course.getCourseName(), course.getCourseMark()));
    courses.add(course);
    UserModel expectedUserModel = new UserModel(user.getId(), user.getCoursesCount(), coursesModel);
    when(userRepository.findById(userId)).thenReturn(user);
    when(coursesRepository.findCoursesByUser(user)).thenReturn(courses);
    //When
    UserModel userModel = dataProviderService.getUserModel(userId);
    //Then
    Assert.assertEquals(expectedUserModel, userModel);
  }
}
