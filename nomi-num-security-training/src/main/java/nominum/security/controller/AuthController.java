package nominum.security.controller;

import java.io.IOException;
import nominum.security.service.AuthServiceImpl;
import nominum.nominumtraining.model.AuthModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

  @PostMapping("/auth")
  public ResponseEntity auth(@RequestBody AuthModel authModel) throws IOException {
    AuthServiceImpl authServiceImpl = new AuthServiceImpl();
    return new ResponseEntity(authServiceImpl.checkAuth(authModel));
  }
}
