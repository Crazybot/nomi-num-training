package nominum.security.service;

import nominum.nominumtraining.model.AuthModel;
import org.springframework.http.HttpStatus;

public interface AuthService {

  HttpStatus checkAuth(AuthModel auth);
}
