package nominum.security.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import nominum.nominumtraining.model.AuthModel;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;

@PropertySource("classpath:users.properties")
public class AuthServiceImpl implements AuthService{

  private Properties userProperties;


  public AuthServiceImpl() throws IOException {
    this.userProperties = new Properties();
    userProperties.load(new FileInputStream(Thread.currentThread().
        getContextClassLoader().getResource("").getPath()+ "users.properties"));
  }

  public HttpStatus checkAuth(AuthModel auth){
    if(userProperties.keySet().contains(auth.getUser())){
      if (userProperties.get(auth.getUser()).equals(auth.getPwd())) {
        return HttpStatus.OK;
      }
    }
    return HttpStatus.UNAUTHORIZED;
  }
}
