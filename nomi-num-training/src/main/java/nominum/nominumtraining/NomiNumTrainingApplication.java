package nominum.nominumtraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class NomiNumTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(NomiNumTrainingApplication.class, args);
	}
}
