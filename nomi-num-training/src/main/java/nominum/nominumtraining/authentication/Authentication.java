package nominum.nominumtraining.authentication;

import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;

public class Authentication {
  @Autowired
  private CsrfTokenRepository csrfTokenRepository;

  public Authentication() {
  }

  public HttpHeaders basicAuthHeaders() {
    String plainCreds = "user:password";
    byte[] plainCredsBytes = plainCreds.getBytes();
    byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
    String base64Creds = new String(base64CredsBytes);

    HttpHeaders headers = new HttpHeaders();
    headers.add("Authorization", "Basic " + base64Creds);
    return headers;
  }

  public HttpHeaders csrfHeaders() {
    CsrfToken csrfToken = csrfTokenRepository.generateToken(null);
    HttpHeaders headers = basicAuthHeaders();
    headers.add(csrfToken.getHeaderName(), csrfToken.getToken());
    headers.add("Cookie", "XSRF-TOKEN=" + csrfToken.getToken());

    return headers;
  }

}
