package nominum.nominumtraining.controller;

import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nominum.nominumtraining.model.AuthModel;
import nominum.nominumtraining.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {


  @Autowired
  AuthenticationService authenticationService;


  @PostMapping("/login")
  public ResponseEntity login(@RequestBody AuthModel authModel, HttpSession httpSession) throws URISyntaxException {
    return authenticationService.login(authModel, httpSession);
  }

  @GetMapping("/logout" )
  public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response) {
    return authenticationService.logout(request, response);
  }
}
