package nominum.nominumtraining.controller;

import java.net.URISyntaxException;
import javax.servlet.http.HttpSession;
import nominum.nominumtraining.services.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Used to handle data transferring requests
 */
@RestController
public class DataController {

  @Autowired
  private DataService dataService;

  /**
   * Method returns user defined by id with list of courses and grades for each
   * @param userId
   * @param httpSession
   * @return
   */
  @GetMapping("/data/{user-id}")
  public ResponseEntity getUser(@PathVariable("user-id") Long userId, HttpSession httpSession)
      throws URISyntaxException {

    return dataService.getUser(userId,httpSession);
  }
}
