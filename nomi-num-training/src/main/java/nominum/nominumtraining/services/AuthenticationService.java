package nominum.nominumtraining.services;

import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nominum.nominumtraining.model.AuthModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface AuthenticationService {

  ResponseEntity login(@RequestBody AuthModel authModel, HttpSession httpSession)
      throws URISyntaxException;

  ResponseEntity logout(HttpServletRequest request, HttpServletResponse response);

}
