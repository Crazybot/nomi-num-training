package nominum.nominumtraining.services;

import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nominum.nominumtraining.model.AuthModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

  @Value("${auth.host}")
  private String authHost;
  @Value("${auth.login}")
  private String authLogin;
  @Value("${auth.password}")
  private String authPassword;


  @Override
  public ResponseEntity login(AuthModel authModel, HttpSession httpSession)
      throws URISyntaxException {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate restTemplate = restTemplateBuilder.build();
    RequestEntity<AuthModel> body = RequestEntity
        .post(new URI("http://" + authHost + "/auth"))
        .accept(MediaType.APPLICATION_JSON)
        .body(authModel);
    restTemplate.getInterceptors().add(
        new BasicAuthorizationInterceptor(authLogin, authPassword));

    ResponseEntity<AuthModel> exchange = restTemplate.exchange(body, AuthModel.class);
    if (exchange.getStatusCode().equals(HttpStatus.OK)) {
      httpSession.setAttribute("Permit", true);
      return new ResponseEntity(HttpStatus.OK);
    } else {
      httpSession.setAttribute("Permit", false);
      return new ResponseEntity(exchange.getStatusCode());
    }

  }

  @Override
  public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }
    return new ResponseEntity(HttpStatus.OK);

  }
}
