package nominum.nominumtraining.services;

import java.net.URISyntaxException;
import javax.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;

public interface DataService {
  ResponseEntity getUser(Long userId, HttpSession httpSession) throws URISyntaxException;
}
