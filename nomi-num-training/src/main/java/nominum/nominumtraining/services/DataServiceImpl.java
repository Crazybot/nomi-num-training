package nominum.nominumtraining.services;

import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataServiceImpl implements DataService {

  @Value("${data.host}")
  private String dataHost;
  @Value("${data.login}")
  private String dataLogin;
  @Value("${data.password}")
  private String dataPassword;

  @Override
  public ResponseEntity getUser(Long userId, HttpSession httpSession) throws URISyntaxException {
    if (httpSession.getAttribute("Permit") != null) {
      if ((boolean) httpSession.getAttribute("Permit")) {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder.build();
        URI uri = new URI("http://" + dataHost + "/data/1");
        return restTemplate.getForEntity(uri, Object.class);
      } else {
        return new ResponseEntity(HttpStatus.FORBIDDEN);
      }
    }
    return new ResponseEntity(HttpStatus.UNAUTHORIZED);

  }
}
